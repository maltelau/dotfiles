#!/home/maltelau/.local/share/python-virtualenv/bin/python3

"""
Download a picture of the day and update desktop background
Malte, august 2023
"""


from bs4 import BeautifulSoup
import requests, re, os, shutil, textwrap


base_url = "https://apod.nasa.gov/apod/"
url = base_url + "astropix.html"
wallpaper_file   = "~/.local/share/wallpaper-astropix.jpg"
explanation_file = "~/.local/share/wallpaper-astropix.txt"

class ImageNotFoundError(ValueError):
    pass

def get_site():
    ## load the pix of the day site
    page_r = requests.get(url).text
    return BeautifulSoup(page_r, "html5lib")

def get_image(page):
    ## find the image
    container = page.select_one('p:-soup-contains("Each day a different image")').find_next_siblings()[0]
    image_url = container.find(href=re.compile("\.jpe?g$"))
    if not image_url or not 'href' in image_url:
        raise ImageNotFoundError("New image not found. Could be a video or a gif?")
    image_s = requests.get(base_url+image_url['href'], stream=True)
    return image_s.raw.read()

def image_is_new(image):
    if os.path.isfile(os.path.expanduser(wallpaper_file)):
        with open(os.path.expanduser(wallpaper_file), 'rb') as f:
            old = f.read()
            if old == image:
                return False
    return True

def save_image(image):
    with open(os.path.expanduser(wallpaper_file), 'w+b') as out_file:
        out_file.write(image)
        # shutil.copyfileobj(image.raw, out_file)

def save_explanation(page):
    ## find the explanation
    text = page.select_one('b:-soup-contains("Explanation")').parent.text[16:].strip()
    wrapped_text = textwrap.fill(text)
    with open(os.path.expanduser(explanation_file), 'w+') as out_file:
        out_file.write(wrapped_text)


def set_background():
    os.system("feh --bg-max " + wallpaper_file)

if __name__ == "__main__":
    page = get_site()
    try:
        image = get_image(page)

        if image_is_new(image):
            print("Found new image")
            save_image(image)
            save_explanation(page)
            set_background()
        else:
            print("Found old image")

    except ImageNotFoundError:
        print("Didn't find a new image")
        set_background()

    #print(len(image))
