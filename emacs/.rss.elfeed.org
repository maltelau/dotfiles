* Blogs                                                              :elfeed:
** Statistics                                                    :statistics:
*** https://andrewgelman.com/feed/
*** https://feeds.feedburner.com/datacolada/
*** http://www.the100.ci/feed/
*** https://thehardestscience.com/feed/
*** https://www.allendowney.com/blog/feed
*** http://models.street-artists.org/feed/
*** http://elevanth.org/blog/feed
*** https://junkcharts.typepad.com/numbersruleyourworld/atom.xml
*** https://junkcharts.typepad.com/junk_charts/atom.xml
*** https://errorstatistics.com/feed/
*** https://www.johndcook.com/blog/feed
*** https://simplystatistics.org/index.xml
*** http://fharrell.com/post/index.xml
*** http://www.biostat.jhsph.edu/%7Erpeng/podcast/simplystatistics_audio.xml :podcast:
*** https://eiko-fried.com/feed
*** https://www.alexpghayes.com/blog/index.xml
*** https://itschancy.wordpress.com/feed/
** Science                                                          :science:
**** https://retractionwatch.com/category/weekend-reads/feed/
**** http://www.raulpacheco.org/blog/feed/
** Danmark                                                               :DK:
*** https://www.dr.dk/nyheder/service/feeds/mostread/viden/         :science:
*** https://www.dr.dk/nyheder/service/feeds/mostread/regionale/oestjylland/
*** https://www.dr.dk/nyheder/service/feeds/politiske-analyser/
** People                                                            :people:
*** https://queryfeed.net/twitter?q=%40fusaroli%2C+from%3Afusaroli&title-type=user-name-both&order-by=recent&geocode=&attach=on :riccardo:statistics:
** Comics                                                             :comic:
*** https://www.smbc-comics.com/comic/rss
*** https://xkcd.com/rss.xml
*** https://www.questionablecontent.net/QCRSS.xml
*** http://www.giantitp.com/comics/oots.rss
*** https://www.erfworld.com/rss
*** http://comicfeeds.chrisbenard.net/view/dilbert/default
*** http://talesfromthevault.com/thunderstruck/rss.xml
*** http://www.girlgeniusonline.com/ggmain.rss
** Reddit							     :reddit:
*** https://old.reddit.com/message/inbox/.rss?feed=9f11ab89260276afe5c8c23ce79ef6c7210f0a75&user=questionquality
*** https://old.reddit.com/message/moderator/inbox/.rss?feed=9f11ab89260276afe5c8c23ce79ef6c7210f0a75&user=questionquality
** Programming                                                  :programming:
*** https://betterdev.link/rss.xml
