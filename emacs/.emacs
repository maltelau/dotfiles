;; -*- lexical-binding: t; -*-
;; -*-no-byte-compile: t; -*-

;; emacs config file
;; Malte Lau Petersen
;; maltelau@protonmail.com

;; TODO: see if there's something interesting in
;; https://github.com/frankjonen/emacs-for-writers/blob/master/.spacemacs#L4

(setq gc-cons-threshold 128000000)
(add-hook 'after-init-hook
	  #'(lambda ()  ;; restore after startup
	      (setq gc-cons-threshold 6400000)))
(setq read-process-output-max (* 1024 1024))

;; (setq server-socket-dir (format "/tmp/emacs%d" (user-uid)))

;;; timestamps in *Messages*                                                    
(defun current-time-microseconds ()
  (let* ((nowtime (current-time))
         (now-ms (nth 2 nowtime)))
    (concat (format-time-string "[%Y-%m-%d %T" nowtime) (format ".%d] " (round now-ms 1000)))))

(defadvice message (before test-symbol activate)
  (unless (string-equal (ad-get-arg 0) "%s%s")
    (let ((deactivate-mark nil)
	  (inhibit-read-only t))
      (with-current-buffer "*Messages*"
	(goto-char (point-max))
	(unless (bolp) (newline))
	(insert (current-time-microseconds))))))


;; ##################################
;; load path
;; (add-to-list 'load-path "~/.emacs.d/elpa/org-20190826")
(let ((default-directory  (expand-file-name "~/.emacs.d/")))
  (normal-top-level-add-subdirs-to-load-path))

;; ######################################
;; MELPA
(require 'package)
(setq inhibit-startup-screen t)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; (add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
;; (package-initialize)
(require 'use-package)


;; (server-start)


;; #####################################
;; ########## Various settings #########
;; #####################################

(setq bidi-display-reordering nil) ;; slow

;; switching buffers
(windmove-default-keybindings 'meta)

;; case-insensitive autocomplete in minibuffer
(setq completion-ignore-case t)
(setq read-buffer-completion-ignore-case t)
(setq read-file-name-completion-ignore-case t)

;; move backup files to .saves
(setq backup-directory-alist `(("." . "~/.saves")))

;; mouse events in margins
(add-hook 'after-init-hook
	  #'(lambda ()
	      (dolist (margin (list "left-margin" "right-margin"))
		(dolist (key (list "mouse-1" "mouse-2" "mouse-3" "mouse-4" "mouse-5"))
		  (dolist (prefix (list "C-" "S-" ""))
		    (define-key global-map
		      (kbd (format "<%s> <%s%s>" margin prefix key))
		      (key-binding (kbd (format "<%s%s>" prefix key)))))))))


; Window title
(setq frame-title-format
      '("Emacs - " (buffer-file-name "%f" (dired-directory dired-directory "%b"))))

;; FONTS
(add-to-list 'default-frame-alist '(font . "Hack"))
;; (set-default-font "Hack")
;; (setq org-src-fontify-natively nil)

; UTF-8 by default
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)

; easier cut and paste (supposedly)
(setq select-active-regions nil)
(setq x-select-enable-primary t)
(setq x-select-enable-clipboard t)
(setq mouse-drag-copy-region nil)

; y or n instead of yes and no
(defalias 'yes-or-no-p 'y-or-n-p)

; something about compilation
(setq load-prefer-newer t)

;; tabs vs spaces
(setq tab-width 4)
(show-paren-mode t)


;; support tilde
(require 'iso-transl)

;; #####################################
;; ########## Packages #################
;; #####################################

(use-package smooth-scroll
  :ensure t
  :config
  (smooth-scroll-mode 0)
  (setq smooth-scroll/vscroll-step-size 2))

;; ########################################
;; magit
(use-package magit
  :defer
  :bind ("C-x g" . magit-status))

(use-package forge
  :ensure t :defer t
  :after magit)

(use-package projectile :ensure t
  :bind ("C-c p" . projectile-command-map)
  :config)

;; (use-package helm-projectile :ensure t
;;   :config
;;   (helm-projectile-on))
;; ###################################
;; helm
;; (use-package helm
;;   :ensure t
;;   :bind (("C-c h" . helm-command-prefix)
;; 	 ("M-x" . helm-M-x)
;; 	 ("M-y" . helm-show-kill-ring)
;; 	 ("C-x b" . helm-mini)
;; 	 ("C-x C-f" . helm-find-files)
;; 	 ("C-x f" . helm-recentf))
;;   :config
;;   (use-package helm-config :demand)
;;   (helm-autoresize-mode t))


(use-package counsel
  :ensure t)

(use-package ivy :ensure t
  :diminish (ivy-mode . "")
  :bind
  (:map ivy-mode-map
		("C-'" . ivy-avy)
		("M-g g" . avy-goto-line)
		("M-x" . counsel-M-x)
		("M-y" . counsel-yank-pop)
		("C-x b" . ivy-switch-buffer)
		("C-x C-f" . counsel-find-file)
		("C-s" . counsel-grep-or-swiper)
		("C-x f" . counsel-recentf))
  :config
  (ivy-mode 1)
  ;; add ‘recentf-mode’ and bookmarks to ‘ivy-switch-buffer’.
  (setq ivy-use-virtual-buffers t)
  ;; number of result lines to display
  (setq ivy-height 10)
  ;; does not count candidates
  (setq ivy-count-format "")
  ;; no regexp by default
  (setq ivy-initial-inputs-alist nil)
  (setq counsel-grep-base-command
 "rg -i -M 120 --no-heading --line-number --color never '%s' %s")
  ;; configure regexp engine.
  (setq ivy-re-builders-alist
	;; allow input not in order
        '((t   . ivy--regex-ignore-order))))


(use-package avy :ensure t)

;;; yasnippet
;; (use-package yasnippet)





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  Dynamic margins for some modes
(defgroup dynamic-margin nil
  "'minor mode' for adding margins in certain major modes when there's space for it.")

(defcustom dynamic-margin/left-margin '(3 . 20)
  "Defines the left margin width.  The CAR is the min, the CDR is the max. 

In a narrow buffer, the only margin at all will be the CAR on the left. 
As the buffer gets wider than the width, the left margin will grow to the CDR.
When the buffer gets wider than the CDR + width, it will start adding right margin."
  :type '(cons integer integer)
  :group 'dynamic-margin)

(defcustom dynamic-margin/desired-width 100
  "Defines the desired with of the text area."
  :type 'integer
  :group 'dynamic-margin)

(defcustom dynamic-margin/modelist (list
					 'emacs-lisp-mode
					 'nov-mode
					 'lisp-interaction-mode)
  "A list of major modes to apply dynamic margins to."
  :type '(repeat symbol)
  :group 'dynamic-margin)

(defcustom dynamic-margin/on t
  "Activate the dynamic margins or not?"
  :type 'boolean
  :group 'dynamic-margin)


(defun toggle-dynamic-margin ()
  (interactive)
  (if dynamic-margin/on
      (setq dynamic-margin/on nil)
    (setq dynamic-margin/on t))
  (dynamic-margin/center-all-windows))



(defun dynamic-margin/center-window (win)
  (when (member (buffer-local-value 'major-mode (window-buffer win))
		dynamic-margin/modelist)
    (if dynamic-margin/on
	;; interpolate left margin according to dynamic-left-margin
	(let ((left-margin (max (min (cdr dynamic-margin/left-margin)
				     (- (window-total-width win) dynamic-margin/desired-width 1))
				(car dynamic-margin/left-margin))))
	  (if (> (window-total-width win) dynamic-margin/desired-width)
	      ;; window is big enough
	      (set-window-margins win
				  left-margin
				  (max 0 (- (window-total-width win)
					    left-margin
					    dynamic-margin/desired-width
					    1)))
	    ;; window is too small to show full desired width
	    (set-window-margins win (car dynamic-margin/left-margin) 0)))
      ;; dynamic-margin isn't on
      (set-window-margins win 0 0))))


(defun dynamic-margin/center-all-windows ()
  (walk-windows (lambda (window) (dynamic-margin/center-window window)) "no-minibuffer" t))
(add-hook 'window-configuration-change-hook 'dynamic-margin/center-all-windows)
;; (remove-hook 'window-configuration-change-hook 'center-all-windows)


;;;;;;;;;;
;; use the total window width rather than the text area (minus margins) to
;; determine when to split vertically
(defun use-full-window-width (fun win &optional horizontal)
  (advice-add 'window-width :override #'window-total-width)
  (prog1
      (funcall fun win horizontal)
    (advice-remove 'window-width #'window-total-width)))

(advice-add 'window-splittable-p :around #'use-full-window-width)
;; (advice-remove 'window-splittable-p #'use-full-window-width)


(use-package calfw
  :ensure t
  :bind ("C-x a" . cfw:open-org-calendar)
  :init
  (setq cfw:org-overwrite-default-keybinding t)
  :config
  (require 'calfw-org)
  (setq calendar-week-start-day 1))


;; Fix terminal colours
;; todo only set when in terminal
(set-background-color "dim gray")
;; (set-face-background 'default "gray10")
;; (set-face-background 'region "gray80")
;; (set-face-foreground 'default "white")
;; (set-face-foreground 'region "gray10")

;; (require 'notmuch-ical)


;; (require 'org-notmuch)



;; (use-package olivetti
;;   :ensure t)



;; ######################################
;; LSP
    ;; (add-hook 'R-mode-hook #'lsp-R-enable)

(use-package lsp-mode
  :ensure t
  :init (setq lsp-keymap-prefix "C-c l")
  :hook ((ess-mode . lsp)
		 (ess-r-mode . lsp))
  :commands lsp
  :config
  (lsp-register-client
   (make-lsp-client
    :new-connection (lsp-stdio-connection
		     '("R" "--slave" "-e" "languageserver::run()"))
    :major-modes '(ess-mode)
    :server-id 'lsp-R))
  (add-to-list 'lsp-language-id-configuration '(ess-mode . "R"))
  (use-package lsp-ui
	:ensure t
	:commands lsp-ui-mode
	:bind (:map lsp-ui-mode-map
				("M-." . #'lsp-ui-peek-find-definitions)
				("M-?" . #'lsp-ui-peek-find-references)))
  (use-package company-lsp
	:ensure t
	:commands company-lsp
	:config (push 'company-lsp company-backends))
  (setq lsp-prefer-capf t))

;; (use-package flycheck :ensure t
;;   :hook ((ess-mode . flycheck)
;; 		 (ess-r-mode . flycheck)))

;; flycheck
(use-package flycheck
  :ensure t
  :defer t
  :config
  (global-flycheck-mode t)
  ;; note that these bindings are optional
  (global-set-key (kbd "C-c n") 'flycheck-next-error)
  ;; this might override a default binding for running a python process,
  ;; see comments below this answer
  (global-set-key (kbd "C-c p") 'flycheck-previous-error)
  )
;; flycheck-pycheckers
;; Allows multiple syntax checkers to run in parallel on Python code
;; Ideal use-case: pyflakes for syntax combined with mypy for typing
(use-package flycheck-pycheckers
  :after flycheck
  :ensure t
  :init
  (with-eval-after-load 'flycheck
    (add-hook 'flycheck-mode-hook #'flycheck-pycheckers-setup)
    )
  (setq flycheck-pycheckers-checkers
    '(
      mypy3
      pyflakes
      )
    )
  :config
  (remove-hook 'elpy-modules 'elpy-module-flymake) ;; <- This removes flymake from elpy
  ;; (setq flycheck-pycheckers-venv-root "~/.miniconda3")
  )
;; elpy
(use-package elpy
  :after poetry
  :ensure t
  :config
  (elpy-enable)
  (add-hook 'elpy-mode-hook 'poetry-tracking-mode) ;; optional if you're using Poetry
  (setq elpy-rpc-virtualenv-path 'current)
  ;; (setq elpy-syntax-check-command "~/.miniconda3/bin/pyflakes") ;; or replace with the path to your pyflakes binary
  ;; allows Elpy to see virtualenv
  (add-hook 'elpy-mode-hook
        ;; pyvenv-mode
        '(lambda ()
           (pyvenv-mode +1)
           )
        )
  ;; use flycheck instead of flymake
  (when (load "flycheck" t t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))
  )
;; poetry
(use-package poetry
  :ensure t)



;; ###################################
;; org-mode

(use-package org
  :defer t
  :bind (;; ("c-x a" . org-agenda)
	 ("C-c c" . org-capture))
  :hook (org-mode . visual-line-mode)
  :init
  (setq org-default-notes-file "~/TODO.org")
  (setq org-agenda-files (list "~/TODO.org" "~/Dokumenter/timetracking.org"))
  (setq org-ellipsis " ⤵") ;; folding symbol
  (setq org-feed-alist
  	'(("IMC"
  	   "http://interactingminds.au.dk/events/rss.xml?element=9567"
  	   "~/TODO.org" "IMC")))
  ;; (capture) todo list
  (setq org-capture-templates '(("t" "Notes" entry
				 (file+headline org-default-notes-file "Tasks")
				 "* TODO %t %?\n %i\n %a")		;template
				("c" "Calendar" entry
				 (file+headline org-default-notes-file "AGENDA")
				 "*** %^{Calendar event:}\n%^T\n%?")
				("i" "Calendar invite" entry
				 '(file+headline org-default-notes-file "Invitations")
				 "** %:event-summary
:PROPERTIES:
:LOCATION: %:event-location
:SEQUENCE: %:event-sequence
:ORGANIZER: [[%:event-organizer]]
:ID: %:event-uid
:END:
%:event-timestamp
%:event-comment
%?")
				("m" "Musik" entry
				 (file+headline org-default-notes-file "Musik")
				 "** %^{Musik}") ))
  (setq org-hide-emphasis-markers t)
  :config
  (org-link-set-parameters
   "mpv"
   :follow (lambda (path)
	     (setq mpv-process (start-process "mpv-process"
				(get-buffer-create "*mpv*")
				"/usr/bin/mpv"
				"--quiet"
				(expand-file-name path))))
   :complete (lambda ()
	       (format "mpv:%s" (read-file-name "Audiofile: "))))
  ;; (org-feed-update-all)
  (add-to-list 'org-latex-packages-alist '("" "listings" nil))
  (add-to-list 'org-latex-packages-alist '("" "minted" nil))
  (setq org-latex-listings 'minted)
  (setq org-latex-listings-options '(("breaklines" "true")))
  (setq org-latex-minted-options '(("breaklines" "true")
                                   ("breakanywhere" "true")
								   ("breaksymbolleft" " ")
								   ;; ("breakaftersymbolpre" " ")
								   ("breakanywheresymbolpre" " ")
								   ("breakindentnchars" "4")
								   ;; ("breaksymbolindentleft" "  ")
								   ))
  (use-package org-ref :ensure t)
  (setq org-startup-indented 1)
  )



(use-package org-download
  :ensure t)



;; pretty unordered lists
(font-lock-add-keywords 'org-mode
                        '(("^ *\\([-]\\) "
						   (0
							(prog1 ()
							  (compose-region (match-beginning 1) (match-end 1) "•"))))))
;; right-arrows for orgmode 🡒 or ↳
;; inspi from http://xahlee.info/comp/unicode_full-width_chars.html
(font-lock-add-keywords 'org-mode
                        '(("^ *\\([-][>]\\) "
						   (0
							(prog1 ()
							  (compose-region (match-beginning 1) (match-end 1) "↳"))))))


;; indented quote blocks



(defface my-org-header '((t :family "EtBemba" :inherit 'default))
  "Org headline face: from tuftecss"
  :group 'org-faces)

(use-package php-mode
  :ensure t)

(use-package org-bullets
  :ensure t
  :hook (org-mode . org-bullets-mode))
;; ("◉" "○" "✸" "✿" "◐" "⊗" "✽" "◌")


(use-package org-download
  :bind ("C-c s" . org-download-screenshot)
  :ensure t)

(use-package org-d20
  :defer t
  :hook (org-mode . (lambda () (require 'org-d20)))
  :ensure t)

(use-package org-variable-pitch
  :ensure t
  :defer t
  :hook (org-mode . org-variable-pitch-minor-mode)
  :init
  ;; add -> to variable pitch mode
  (setq org-variable-pitch-font-lock-keywords
	;; taken from org-variable pitch
	(let ((code '(0 (put-text-property
			 (match-beginning 0)
			 (match-end 0)
			 'face 'org-variable-pitch-face))))
	  `((,(rx bol (1+ blank))
	     ,code)
	    (,(rx bol (0+ blank)
		  (or (: (or (+ digit) letter) (in ".)"))
		      (: (in "-+")
			 (opt ">") ;; modified: -> rightarrow
			 (opt blank "[" (in "-X ") "]"))
		      (: (1+ blank) "\*"))
		  blank)
	     ,code)))))

;; dnd style org -> pdf exports
(use-package ox-dnd
  :hook (org-mode . (lambda () (require 'ox-dnd)))
  :defer t)

;; (use-package org-latex
;;   :defer t
;;   :ensure t)

(use-package nov :ensure t :defer t
  :mode ("\\.epub\\'" . nov-mode))

(use-package festival
  :defer t
  :bind (("C-c r" . #'festival-say-region)
	 ("C-c C-r" . #'festival-kill-process))
  :config
  (defun my-festival-voice ()
    (process-send-string
     festival-process
     "(voice_cmu_us_slt_arctic_hts)"))
  (advice-add
   #'festival-start-process :after
   #'my-festival-voice)
  (festival-start-process))


  ;; (defun maltelau/say-from-point ()
  ;; )
    ;; (while t
    ;;   (move-beginning-of-line 1)
    ;;   (write-region (point-at-bol) (point-at-eol) festival-tmp-file)
    ;;   (festival-send-command (list 'tts festival-tmp-file nil))
    ;;   (forward-line)))



;; (defun my-festival-start-process ()
;;   "Check status of process and start it if necessary"
;;   (interactive)
;;   (let ((process-connection-type t))
;;     (if (and festival-process
;;             (eq (process-status festival-process) 'run))
;;         't
;;       ;;(festival-kill-festival t)
;;       (message "Starting new synthesizer process...")
;;       (sit-for 0)
;;       (setq festival-process
;;             (start-process "festival" (get-buffer-create "*festival*")
;;                            festival-program-name "--server"))
;;       (set-process-coding-system festival-process 'iso-latin-1 'iso-latin-1)
;;       festival-process)))

;; (defun my-festival-play-line ()
;;   "Play one line through festival"
;;   (interactive)
;;   (when (bound-and-true-p festival-process)
;;     (while (not (equal (point) (point-max)))
;;       (let ((coding-system-for-write 'iso-latin-1))
;; 	(write-region (point-at-bol) (point-at-eol) festival-tmp-file))
;;       (call-process-shell-command
;;        (format "festival_client --ttw %s | aplay" festival-tmp-file) nil "*festival-client*")
;;       (forward-line 1))))

;; (message (festival-ready-p))
;; (defun festival-ready-p ()
;;   (interactive)
;;     (when (bound-and-true-p festival-process)
;;       (with-current-buffer "*festival*"
;; 	(goto-char (point-max))
;; 	(string= "festival> "
;; 	 (buffer-substring-no-properties
;; 	  (point-at-bol)
;; 	  (point-at-eol))))))

(global-set-key "\C-cm" 'compile)
  
;; (advice-add 'festival-start-process :after #'festival-cmu)
;; (advice-remove 'festival-start-process #'festival-cmu)

;; ;; setup variable pitch font
;; (defun modi/font-check ()
;;   "Do font check, then remove self from `focus-in-hook'; need to run this just once."
;;   (require 'setup-font-check)
;;   (remove-hook 'focus-in-hook #'modi/font-check))
;; (add-hook 'focus-in-hook #'modi/font-check)


(use-package org-lookup-dnd
  :bind ("C-c d" . org-lookup-dnd-at-point)
  :config (require 'org-pdfview))

;; (use-package org-pdfview :ensure t)

;; (use-package pdf-tools
;;  :pin manual ;; manually update
;;  :config
;;  ;; initialise
;;  (pdf-tools-install)
;;  ;; open pdfs scaled to fit page
;;  (setq-default pdf-view-display-size 'fit-page)
;;  ;; automatically annotate highlights
;;  (setq pdf-annot-activate-created-annotations t)
;;  ;; use normal isearch
;;  (define-key pdf-view-mode-map (kbd "C-s") 'isearch-forward))


;;HERE
;; ############
;; org-mode to pdf
;; TODO Fix bibliography path
(use-package pandoc-mode
  :ensure t
  :config
  ;; (add-hook 'markdown-mode-hook 'pandoc-mode)
  (setq org-latex-pdf-process
	'("latexmk -pdflatex='pdflatex -shell-escape -interaction=nonstopmode' -pdf -bibtex %b -f %f"))
  (setq reftex-default-bibliography '("/home/maltelau/Dokumenter/CogSci/zotero.bib"))
  (setq bibtex-completion-library-path '("/home/maltelau/Dokumenter/bibtex/pdf"))
  (setq org-ref-default-bibliography '("/home/maltelau/Dokumenter/CogSci/zotero.bib"))
  (setq org-ref-bibtex-files '("/home/maltelau/Dokumenter/CogSci/zotero.bib"))
  (setq org-ref-pdf-directory '("/home/maltelau/Dokumenter/bibtex/pdf"))
  (global-set-key "\C-c]" 'org-ref-ivy-insert-cite-link)) ;; TODO find a better shortcut
;; (require 'pandoc-mode)

(defun toggle-org-pdf-export-on-save ()
  (interactive)
  (if (memq 'org-latex-export-to-pdf after-save-hook)
      (progn
        (remove-hook 'after-save-hook 'org-latex-export-to-pdf t)
        (message "Disabled org html export on save for current buffer..."))
    (add-hook 'after-save-hook 'org-latex-export-to-pdf nil t)
    (message "Enabled org html export on save for current buffer...")))


;; (use-package wc-mode :ensure t)

;;(add-hook 'pandoc-mode-hook 'pandoc-load-default-settings)

;; ############
;; other things to pdf
;; BEAMER
;; (eval-after-load "ox-latex"
;;   '(add-to-list 'org-latex-classes
;; 		`("beamer"
;; 		  ,(concat "\\documentclass[presentation]{beamer}\n"
;; 			   "[DEFAULT-PACKAGES]"
;; 			   "[PACKAGES]"
;; 			   "[EXTRA]\n")
;; 		  ("\\section{%s}" . "\\section*{%s}")
;; 		  ("\\subsection{%s}" . "\\subsection*{%s}")
;; 		  ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))


;; ;; org-latex apa style
;; (require 'ox-latex)
;; (use-package ox-latex
;;   :ensure t)

;; (with-eval-after-load 'ox-latex
;;   (require 'org-ref-latex)
;;   (add-to-list 'org-latex-classes
;; 	       '("apa6"
;; 		 "\\documentclass[hidelinks,a4paper,natbib,jou]{apa6}"
;; 		 ("\\section{%s}" . "\\section*{%s}")
;; 		 ("\\subsection{%s}" . "\\subsection*{%s}")
;; 		 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
;; 		 ("\\paragraph{%s}" . "\\paragraph*{%s}")
;; 		 ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))



;; ####################################
;; misc features


;; ;; Project folders
;; (use-package projectile
;;   :bind ("C-c p" . projectile-mode)
;;   :bind-keymap ("C-c p" . projectile-command-map)
;;   :ensure t
;;   :demand
;;   :config (use-package projectile-ripgrep
;; 	    :ensure t
;; 	    :demand
;; 	    :bind (:map projectile-mode-map
;; 			"C-s" projectile-ripgrep)))



;; annotating pdfs and ebooks
(use-package org-noter
  :ensure t)

;; ;; opening pdf files
(use-package pdf-tools
  ;; :ensure t
  :load-path "site-lisp/pdf-tools/lisp"
  :magic ("%PDF" . pdf-view-mode)
  :config
  (pdf-tools-install))

;; (use-package openwith
;;   :config
;;   (openwith-mode t)
;;   (setq openwith-associations '(("\\.pdf\\'" "evince" (file)))))

;; line numbers everywhere
;; (global-linum-mode nil)

;; recent files
(use-package recentf :defer t
  :config (progn (recentf-mode 1)
		 (run-at-time nil (* 60 20) 'recentf-save-list))
  ;; :bind ("C-x f" . 'recentf-open-files)
  )

;; (use-package auto-complete :ensure t
;;   :config (ac-config-default))

(use-package biblio :ensure t)

;; ######################
;; RSS via elfeed
(use-package elfeed
  :ensure t
  :custom-face
  ;; (statistics-elfeed-entry ((t (:foreground "#f77"))))
  ;; (science-elfeed-entry ((t (:foreground "#8ac6f2"))))
  :bind ("C-x w" . 'elfeed)
  :init
  (setq-default elfeed-search-filter "@2-weeks-ago +unread ")
  (defface statistics-elfeed-entry '((t :foreground "#f77")) "marks stats blogs")
  (defface science-elfeed-entry '((t :foreground "#8ac6f2")) "marks science blogs")
  (setq rmh-elfeed-org-files (list "~/.rss.elfeed.org"))
  :config
  (add-hook 'kill-emacs-hook 'elfeed-db-compact)
  (push '(statistics statistics-elfeed-entry) elfeed-search-face-alist)
  (push '(science science-elfeed-entry) elfeed-search-face-alist)
  (use-package elfeed-org :demand :ensure t
    :config (elfeed-org))
  (use-package elfeed-goodies :demand :ensure t
    :after (elfeed elfeed-org)
    :bind ((:map elfeed-show-mode-map ("l" . elfeed-goodies/toggle-logs))
  	   (:map elfeed-search-mode-map ("l" . elfeed-goodies/toggle-logs)))
    :config (elfeed-goodies/setup)))



;; ####################################
;; mail sync config

(use-package notmuch
  :bind (("C-x m" . notmuch)
		 :map notmuch-search-mode-map
		 ("S" . (lambda () (interactive) (notmuch-search-tag (list "+spam" "-inbox" "-unread"))))
		 ("D" . (lambda () (interactive) (notmuch-search-tag (list "+deleted" "-inbox" "-unread"))))
		 ("R" . (lambda () (interactive) (notmuch-search-tag (list "-unread" "-todo"))))
		 ("U" . (lambda () (interactive) (notmuch-search-tag (list "+unread"))))
		 ("T" . (lambda () (interactive) (notmuch-search-tag (list "+todo"))))
		 :map notmuch-show-mode-map
		 ("S" . (lambda () (interactive) (notmuch-show-tag (list "+spam" "-inbox" "-unread"))))
		 ("D" . (lambda () (interactive) (notmuch-show-tag (list "+deleted" "-inbox" "-unread"))))
		 ("R" . (lambda () (interactive) (notmuch-show-tag (list "-unread" "-todo"))))
		 ("U" . (lambda () (interactive) (notmuch-show-tag (list "+unread"))))
		 ("T" . (lambda () (interactive) (notmuch-show-tag (list "+todo"))))
		 ("C-<return>" . (lambda () (interactive) (let ((current-prefix-arg 4))
	    	(call-interactively 'notmuch-show-open-or-close-all 'nil)))))
  
  :hook (((notmuch-hello-refresh notmuch-hello-mode) . sync-mail))
  :init
  (defun sync-mail ()
    "Connect and download incoming mail."
    (start-process-shell-command "systemctl --user start tjek-mail.service" nil "mail service")
	(start-process-shell-command "systemctl --user start tjek-mail.timer" nil "mail timer"))
  (defun start-protonmail-bridge ()
    "Start protonmail bridge for sendmail."
    (start-process-shell-command
     "protonbridge" nil "/usr/bin/protonmail-bridge --no-window --noninteractive")
    (sleep-for 2))
  ;; :config
  ;; (require 'notmuch-agenda)
  )

(use-package message
  :defer t)

(use-package gnus-alias
  :ensure t
  :defer t
  :hook (message-setup . gnus-alias-determine-identity)
  :init
  (setq gnus-alias-identity-alist
		'(("pm" nil "Malte Lau Petersen <maltelau@protonmail.com>")
		  ("dk" nil "Malte Lau Petersen <maltelau@mlpetersen.dk>")
		  ("au" nil "Malte Lau Petersen <201505524@post.au.dk>")))
  (setq gnus-alias-default-identity "dk")
  (setq gnus-alias-identity-rules
	'(("au" ("any" "201505524@post\\.au\\.dk" both) "au")
	  ("pm" ("any" "maltelau@protonmail\\.com" both) "dk")
	  ("dk" ("any" "maltelau@mlpetersen\\.dk" both) "dk"))))

(setq send-mail-function 'sendmail-send-it
      sendmail-program "/usr/bin/msmtp"
      mail-specify-envelope-from t
      message-sendmail-envelope-from 'header
      mail-envelope-from 'header)
;; #######################################
;; programming in general


;; ####################################
;; programming in R

;; ##########
;; Outline mode (for folding code in rmarkdown)
(use-package outline
  :defer
  :bind (:map outline-minor-mode-map
			  ("C-<tab>" . outline-cycle))
  :hook (ess-mode . outline-minor-mode)
  :config
  (setq outline-regexp "\\(^#\\{4,5\\} \\)\\|\\(^[a-zA-Z0-9_\.]+ ?<- ?function(.*{\\)")
  (defun outline-level ()
    (cond ((looking-at "^##### ") 1)
		  ((looking-at "^#### ") 2)
		  ((looking-at "^[a-zA-Z0-9_\.]+ ?<- ?function(.*{") 3)
		  (t 1000))))
(eval-after-load 'outline
  '(progn
    (require 'outline-magic)
    (define-key outline-minor-mode-map (kbd "C-<tab>") 'outline-cycle)))

;; (use-package outline-magic
;;   :after outline)

;; search history
(use-package comint
  :bind (:map comint-mode-map
	      ("C-<up>"   . 'comint-previous-matching-input-from-input)
	      ("C-<down>" . 'comint-next-matching-input-from-input)))

;; (use-package company-box
;;   :hook (company-mode . company-box-mode))

(use-package company
  :hook (ess-mode . company-mode)
  ;; :bind (:map company-mode-map
  ;; 			  ("<tab>" . 'company-complete))
  :defer t :ensure t)


(use-package csv-mode
  :defer t :ensure t
  :mode "\\.csv")


;; (defun my-ess-settings ())

(defun my/add-pipe ()
  "Adds a pipe operator %>% with one space to the left and then
starts a newline with proper indentation"
  (interactive)
  (just-one-space 1)
  (insert "%>%")
  (ess-newline-and-indent))

;; (use-package poly-markdown :ensure t :defer t)
;; (use-package poly-R :ensure t :defer t)
;; ;; (use-package ess :ensure t :defer t)
;; ;; (use-package ess-smart-underscore :defer t)

;; Emacs for R
(defun Rmd-mode ()			
  "ESS Markdown mode for Rmd files"
  (interactive)
  (require 'poly-R)
  (require 'poly-markdown)
  (poly-markdown+r-mode))

(use-package polymode
  :defer t
  :ensure t
  ;; :after (ess-site markdown-mode poly-R poly-markdown)
  :mode (("\\.Snw" . poly-noweb+r-mode)
	 ("\\.Rnw" . poly-noweb+r-mode)
	 ("\\.[rR]md" . Rmd-mode)))


(use-package markdown-mode
  :defer t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :config (setq markdown-enable-math t))

(use-package ess-site
  ;; :ensure t
  :defer t
  ;; :init (setq ess-smart-S-assign-key nil)
  :bind ((:map ess-mode-map
			   ("M-p" . my/add-pipe)
			   ("TAB" . 'company-indent-or-complete-common))
		 (:map inferior-ess-mode-map
			   ("M-p" . my/add-pipe)
			   ("TAB" . 'company-indent-or-complete-common)))
  :config
  (progn
	(ess-set-style 'OWN 'quiet)
	(outline-minor-mode nil)
	;; (setq ess-smart-S-assign-key nil)
	(ess-toggle-underscore nil)
	(setq ess-insert-assign nil)
	(setq ess-smart-S-assign nil)
	(ess-toggle-S-assign nil)
	(ess-toggle-S-assign nil)
	'(ess-smart-S-assign-key nil)
	(setq ess-indent-level 4)
	(setq ess-arg-function-offset 4)
	(setq ess-else-offset 4)
	(setq ess-use-flymake nil) ;; disable Flymake

	(org-babel-do-load-languages
	 'org-babel-load-languages
	 '((R . t)))))

;; (define-key ess-mode-map (kbd "M-p") 'my/add-pipe))


;; this feels weird, but YES it needs to be called twice
;; (ess-toggle-S-assign nil)
;; (ess-toggle-S-assign nil)

;; https://stackoverflow.com/questions/2710442/in-ess-emacs-how-can-i-get-the-r-process-buffer-to-scroll-to-the-bottom-after-a


;; (autoload 'markdown-mode "markdown-mode"
;;   "Major mode for editing Markdown files" t)
;;; MARKDOWN
;;; R modes
;; (add-to-list 'auto-mode-alist '("\\.Snw" . poly-noweb+r-mode))
;; (add-to-list 'auto-mode-alist '("\\.Rnw" . poly-noweb+r-mode))
;;; rmd mode for knitr and compatibility with rstudio


;; (add-hook 'ess-mode-hook 'my-ess-settings)
;; (add-hook 'inferior-ess-mode-hook 'my-ess-settings)

;; knitting

;; large files
(use-package
  vlf
  :ensure t)

;; stan mode
(use-package stan-mode :ensure t :defer t
  :mode "\\.stan")

;; ####################################
;; python 3
;; ##########################3
;; elpy for python dev
;; (use-package elpy
;;   :ensure t
;;   :defer t
;;   :after python
;;   :init
;;   (add-hook 'elpy-mode-hook (lambda () (highlight-indentation-mode 1)))
;;   ;; :command elpy-enable
;;   ;; :mode ("\\.py" . elpy-enable)
;;   ;; :config
;;   ;; (elpy-enable)
;;   )




(org-babel-do-load-languages
 'org-babel-load-languages '((python . t)))
(setq org-babel-python-command "python3")

;; (use-package conda
;;   :ensure t
;;   :defer t
;;   :init
;;   (setq conda-anaconda-home (expand-file-name "~/.miniconda3"))
;;   (setq conda-env-home-directory (expand-file-name "~/.miniconda3"))
;;   :config
;;   (advice-add 'python-mode :after #'conda--switch-buffer-auto-activate)
;;   ;; :hook  (python-mode . )
;;   )

(use-package python
  :ensure t
  :defer t
  ;; :init (elpy-enable)
  :mode ("\\.py" . python-mode)
  :config
  (setq python-shell-interpreter "ipython3"
	python-shell-interpreter-args "--simple-prompt -i -pylab")
  (elpy-enable)
  (subword-mode)
  )


;; in python, send a line or region
;; https://stackoverflow.com/questions/35934972/emacs-elisp-send-line-if-no-region-active-python-mode#35953386

;; (use-package whole-line-or-region
;; 	     :ensure t
;; 	     :diminish whole-line-or-region-mode
;; 	     :config
;; 	     (whole-line-or-region-mode t)
;; 	     (make-variable-buffer-local 'whole-line-or-region-mode))

;; ;; python: ctrl+enter & ctrl+shift+enter shortcuts

;; (defun add-python-keys ()
;;   (local-set-key (kbd "C-c C-c") 'python-shell-send-region)
;;   (local-set-key (kbd "C-S-c C-S-c") 'python-shell-send-buffer))

;; (add-hook 'python-mode-hook 'add-python-keys)

;; (use-package material-theme :ensure t)
(use-package monokai-theme :ensure t)


;; #######################################
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ac-quick-help-delay 0.5)
 '(ac-quick-help-prefer-pos-tip t)
 '(ac-use-quick-help nil)
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(comint-move-point-for-output t)
 '(comint-scroll-to-bottom-on-input t)
 '(company-abort-manual-when-too-short t)
 '(company-box-icons-functions
   '(company-box-icons--yasnippet company-box-icons--lsp company-box-icons--elisp company-box-icons--acphp))
 '(company-idle-delay 0.1)
 '(company-minimum-prefix-length 2)
 '(company-quickhelp-color-background "#4F4F4F")
 '(company-quickhelp-color-foreground "#DCDCCC")
 '(company-selection-wrap-around t)
 '(company-tooltip-align-annotations t)
 '(compilation-message-face 'default)
 '(completion-styles '(basic partial-completion emacs22))
 '(conda-env-autoactivate-mode t)
 '(counsel-grep-swiper-limit 30000000)
 '(csv-invisibility-default nil)
 '(current-language-environment "UTF-8")
 '(custom-enabled-themes '(modus-vivendi))
 '(custom-safe-themes
   '("78e6be576f4a526d212d5f9a8798e5706990216e9be10174e3f3b015b8662e27" "f3ab34b145c3b2a0f3a570ddff8fabb92dafc7679ac19444c31058ac305275e1" "04232a0bfc50eac64c12471607090ecac9d7fd2d79e388f8543d1c5439ed81f5" "bd7b7c5df1174796deefce5debc2d976b264585d51852c962362be83932873d9" "a24c5b3c12d147da6cef80938dca1223b7c7f70f2f382b26308eba014dc4833a" "732b807b0543855541743429c9979ebfb363e27ec91e82f463c91e68c772f6e3" "98cc377af705c0f2133bb6d340bf0becd08944a588804ee655809da5d8140de6" "5dc0ae2d193460de979a463b907b4b2c6d2c9c4657b2e9e66b8898d2592e3de5" "bfdcbf0d33f3376a956707e746d10f3ef2d8d9caa1c214361c9c08f00a1c8409" "f5b591870422cd28da334552aae915cdcae3edfcfedb6653a9f42ed84bbec69f" default))
 '(debug-on-error nil)
 '(display-fill-column-indicator-column 80)
 '(dynamic-margin/desired-width 130)
 '(dynamic-margin/modelist '(emacs-lisp-mode nov-mode python-mode))
 '(dynamic-margin/on t)
 '(elfeed-goodies/entry-pane-position 'bottom)
 '(elfeed-goodies/entry-pane-size 0.6)
 '(elfeed-goodies/feed-source-column-width 20)
 '(elfeed-goodies/log-window-size 0.1)
 '(elfeed-goodies/show-mode-padding 0)
 '(elfeed-goodies/switch-to-entry nil)
 '(elfeed-goodies/tag-column-width 15)
 '(elfeed-goodies/wide-threshold 0.7)
 '(elfeed-sort-order 'ascending)
 '(elpy-dedicated-shells t)
 '(elpy-get-info-from-shell t)
 '(elpy-mode-hook '((lambda nil (highlight-indentation-mode 1))))
 '(elpy-modules
   '(elpy-module-company elpy-module-eldoc elpy-module-flymake elpy-module-folding elpy-module-pyvenv elpy-module-yasnippet elpy-module-django elpy-module-sane-defaults))
 '(elpy-project-ignored-directories '(".tox" "build" "dist" ".cask" ".ipynb_checkpoints"))
 '(elpy-remove-modeline-lighter t)
 '(elpy-syntax-check-command "/home/maltelau/.local/bin/pyflakes")
 '(elpy-test-pytest-runner-command '("pytest"))
 '(ess-R-readline t)
 '(ess-own-style-list
   '((ess-indent-offset . 4)
	 (ess-offset-arguments . open-delim)
	 (ess-offset-arguments-newline . prev-call)
	 (ess-offset-block . prev-call)
	 (ess-offset-continued . straight)
	 (ess-align-nested-calls "ifelse")
	 (ess-align-arguments-in-calls "function[ \11]*(")
	 (ess-align-continuations-in-calls . t)
	 (ess-align-blocks control-flow)
	 (ess-indent-from-lhs arguments fun-decl-opening)
	 (ess-indent-from-chain-start . t)
	 (ess-indent-with-fancy-comments)))
 '(ess-r-flymake-linters
   '("closed_curly_linter = NULL" "commas_linter = NULL" "commented_code_linter = NULL" "infix_spaces_linter = NULL" "line_length_linter = NULL" "object_length_linter = NULL" "object_name_linter = NULL" "object_usage_linter = NULL" "open_curly_linter = NULL" "pipe_continuation_linter = NULL" "single_quotes_linter = NULL" "spaces_inside_linter = NULL" "spaces_left_parentheses_linter = NULL" "trailing_blank_lines_linter = NULL" "trailing_whitespace_linter = NULL" "indentation_linter = NULL"))
 '(ess-use-auto-complete nil)
 '(ess-use-flymake nil)
 '(fci-rule-color "#3C3D37")
 '(flycheck-lintr-linters
   "linters_with_defaults(indentation_linter = NULL, absolute_path_linter = NULL, line_length_linter = NULL, object_usage_linter = NULL, object_name_linter = NULL, commented_code_linter = NULL, undesirable_function_linter = NULL, nonportable_path_linter = NULL, implicit_integer_linter = NULL, extraction_operator_linter = NULL, defaults = linters_with_tags(c(\"default\")))")
 '(fringe-mode 0 nil (fringe))
 '(helm-completing-read-dynamic-complete t)
 '(highlight-changes-colors '("#FD5FF0" "#AE81FF"))
 '(highlight-tail-colors
   '(("#3C3D37" . 0)
	 ("#679A01" . 20)
	 ("#4BBEAE" . 30)
	 ("#1DB4D0" . 50)
	 ("#9A8F21" . 60)
	 ("#A75B00" . 70)
	 ("#F309DF" . 85)
	 ("#3C3D37" . 100)))
 '(hl-sexp-background-color "#efebe9")
 '(inferior-ess-r-program "R")
 '(inhibit-startup-screen t)
 '(initial-buffer-choice nil)
 '(ispell-dictionary nil)
 '(large-file-warning-threshold 200000000)
 '(line-spacing nil)
 '(lsp-ui-doc-header nil)
 '(lsp-ui-doc-include-signature t)
 '(lsp-ui-doc-max-height 20)
 '(lsp-ui-doc-position 'top)
 '(lsp-ui-peek-always-show t)
 '(lsp-ui-peek-list-width 30)
 '(lsp-ui-sideline-show-hover t)
 '(magit-diff-use-overlays nil)
 '(mail-send-hook '(start-protonmail-bridge))
 '(menu-bar-mode nil)
 '(message-cite-style 'message-cite-style-outlook)
 '(message-send-hook
   '(notmuch-message-apply-queued-tag-changes notmuch-draft--mark-deleted start-protonmail-bridge))
 '(mode-require-final-newline t)
 '(mouse-scroll-delay 0.35)
 '(mouse-wheel-progressive-speed nil)
 '(mouse-wheel-scroll-amount '(5 ((shift) . 20) ((control))))
 '(native-comp-async-report-warnings-errors 'silent)
 '(notmuch-always-prompt-for-sender t)
 '(notmuch-fcc-dirs "sent +sent -unread -inbox")
 '(notmuch-hello-auto-refresh t)
 '(notmuch-hello-hide-tags '("all mail"))
 '(notmuch-message-headers-visible nil)
 '(notmuch-saved-searches
   '((:name "inbox" :query "tag:inbox AND NOT tag:delete" :key "i")
	 (:name "unread" :query "tag:unread AND tag:inbox AND NOT tag:delete" :key "u")
	 (:name "flagged" :query "tag:flagged AND NOT tag:delete" :key "f")
	 (:name "sent" :query "tag:sent AND NOT tag:delete" :key "t")
	 (:name "drafts" :query "tag:draft AND NOT tag:delete" :key "d")
	 (:name "all mail" :query "NOT tag:delete" :key "a")))
 '(notmuch-search-oldest-first nil)
 '(notmuch-show-logo nil)
 '(nrepl-message-colors
   '("#CC9393" "#DFAF8F" "#F0DFAF" "#7F9F7F" "#BFEBBF" "#93E0E3" "#94BFF3" "#DC8CC3"))
 '(olivetti-body-width 100)
 '(olivetti-max-left-margin 1.0)
 '(org-adapt-indentation t)
 '(org-agenda-files '("~/TODO.org"))
 '(org-bullets-bullet-list '(" "))
 '(org-bullets-face-name 'Hack)
 '(org-clock-clocktable-default-properties '(:maxlevel 3 :scope file))
 '(org-clock-idle-time 10)
 '(org-clock-mode-line-total 'today)
 '(org-clock-out-remove-zero-time-clocks t)
 '(org-clock-persist t)
 '(org-clock-report-include-clocking-task t)
 '(org-clocktable-defaults
   '(:maxlevel 3 :lang "en" :scope file :block nil :wstart 1 :mstart 1 :tstart nil :tend nil :step nil :stepskip0 nil :fileskip0 nil :tags nil :emphasize nil :link nil :narrow 40! :indent t :formula nil :timestamp nil :level nil :tcolumns nil :formatter nil))
 '(org-feed-default-template
   "\12* IMC %category: %h\12  %(format-time-string (org-time-stamp-format t nil) (org-read-date t t \"%eventStart\"))-%(format-time-string (org-time-stamp-format t nil) (org-read-date t t \"%eventEnd\")) %eventPlace\12  %description\12  %a")
 '(org-fontify-done-headline nil)
 '(org-fontify-quote-and-verse-blocks t)
 '(org-fontify-whole-heading-line nil)
 '(org-indent-indentation-per-level 4)
 '(org-latex-default-packages-alist
   '(("AUTO" "inputenc" t nil)
	 ("T1" "fontenc" t nil)
	 ("" "fixltx2e" nil nil)
	 ("" "graphicx" t nil)
	 ("" "longtable" nil nil)
	 ("" "float" nil nil)
	 ("" "wrapfig" nil nil)
	 ("" "rotating" nil nil)
	 ("normalem" "ulem" t nil)
	 ("" "amsmath" t nil)
	 ("" "textcomp" t nil)
	 ("" "marvosym" t nil)
	 ("" "wasysym" t nil)
	 ("" "amssymb" t nil)
	 ("hyphens" "url" nil nil)
	 ("colorlinks=true, linkcolor=black, urlcolor=blue" "hyperref" nil nil)
	 "\\tolerance=1000"))
 '(org-latex-hyperref-template nil)
 '(org-link-search-must-match-exact-headline t)
 '(org-lookup-dnd-chose 'org-lookup-dnd-chose-helm)
 '(org-lookup-dnd-db-file "~/.local/share/org-lookup-dnd-db.el")
 '(org-lookup-dnd-extra-index "~/.local/share/org-lookup-dnd-extra.org")
 '(org-lookup-dnd-sources
   '(("~/Dropbox/dnd/rules/monsters/DnD 5e Monster Manual.pdf" 1 4 4)
	 ("~/Dropbox/dnd/rules/monsters/Volo's Guide to Monsters.pdf" 1 4 4)
	 ("~/Dropbox/dnd/adventures/Lost Mine of Phandelver.pdf" 0 1 1)
	 ("~/Dropbox/dnd/rules/Players Handbook.pdf" 1 4 4)
	 ("~/Dropbox/dnd/rules/D&D 5E Starter Set Rulebook.pdf" 0 1 1)
	 ("~/Dropbox/dnd/character sheets/allspells.pdf" 0 1 4)
	 ("~/Dropbox/dnd/rules/D&D 5E- Xanathar's Guide To Everything.pdf" 1 4 4)
	 ("~/Dropbox/dnd/rules/DnD 5e Dungeon Masters Guide.pdf" 0 3 3)
	 ("~/Dropbox/dnd/rules/Sword Coast Adventurer's Guide-Wizards of the Coast (2015).pdf" 1 4 4)
	 ("~/Dropbox/dnd/rules/monsters/Mordenkainen’s Tome of Foes.pdf" 1 4 4)
	 ("~/Dropbox/dnd/rules/monsters/tomeofhorrors.pdf" 1 3 5)
	 ("~/Dropbox/dnd/adventures/campains/Storm King's Thunder.pdf" 1 4 5)))
 '(org-pretty-entities t)
 '(org-src-tab-acts-natively t)
 '(org-startup-truncated nil)
 '(org-support-shift-select t)
 '(org-variable-pitch-fixed-font "Hack")
 '(outline-minor-mode-cycle t)
 '(package-check-signature 'allow-unsigned)
 '(package-selected-packages
   '(vlf message notmuch help-find-org-mode org2blog company markdown-mode org-bullets poly-R poly-markdown poly-noweb polymode xmind-org lua-mode magit ess org avy diminish org-lookup-dnd poetry flycheck-pycheckers haskell-mode projectile-ripgrep ripgrep ag company-lsp lsp-ui lsp-mode org-ref wc-mode YASnippet yasnippet-classic gnu-elpa-keyring-update org-pdfview org-download org-noter php-mode org-plus-contrib calfw-org calfw calw magit-forge festival nov org-ebook smtpmail-multi gnus-alias org-link-minor-mode org-variable-pitch smooth-scroll el-get package-lint-flymake flycheck-package flycheck elisp-format selectric-mode use-package))
 '(pdf-view-bounding-box-margin 0.05)
 '(pdf-view-continuous nil)
 '(pdf-view-midnight-colors '("#DCDCCC" . "#383838"))
 '(pdf-view-use-imagemagick t)
 '(polymode-exporter-output-file-format "%s")
 '(polymode-weaver-output-file-format "%s")
 '(pos-tip-background-color "#A6E22E")
 '(pos-tip-foreground-color "#272822")
 '(python-shell-interpreter-args "-i --simple-prompt")
 '(recentf-exclude '("recentf" "elfeed.index"))
 '(recentf-max-saved-items 50)
 '(require-final-newline t)
 '(scroll-bar-mode nil)
 '(send-mail-function 'sendmail-send-it)
 '(sendmail-program "/usr/bin/msmtp")
 '(server-client-instructions nil)
 '(split-height-threshold 80)
 '(sublimity-disabled-major-modes '(shell-mode helm-mode))
 '(tab-always-indent 'complete)
 '(tab-width 4)
 '(tool-bar-mode nil)
 '(transient-save-history nil)
 '(vc-annotate-background nil)
 '(vc-annotate-color-map
   '((20 . "#F92672")
	 (40 . "#CF4F1F")
	 (60 . "#C26C0F")
	 (80 . "#E6DB74")
	 (100 . "#AB8C00")
	 (120 . "#A18F00")
	 (140 . "#989200")
	 (160 . "#8E9500")
	 (180 . "#A6E22E")
	 (200 . "#729A1E")
	 (220 . "#609C3C")
	 (240 . "#4E9D5B")
	 (260 . "#3C9F79")
	 (280 . "#A1EFE4")
	 (300 . "#299BA6")
	 (320 . "#2896B5")
	 (340 . "#2790C3")
	 (360 . "#66D9EF")))
 '(vc-annotate-very-old-color nil)
 '(vc-follow-symlinks t)
 '(warning-suppress-types '((comp)))
 '(wc-modeline-format "Tegn[%tc]")
 '(weechat-color-list
   (unspecified "#272822" "#3C3D37" "#F70057" "#F92672" "#86C30D" "#A6E22E" "#BEB244" "#E6DB74" "#40CAE4" "#66D9EF" "#FB35EA" "#FD5FF0" "#74DBCD" "#A1EFE4" "#F8F8F2" "#F8F8F0")))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :extend nil :stipple nil :background "gray10" :foreground "white" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 127 :width normal :family "Hack"))))
 '(cursor ((t (:background "white" :foreground "sienna" :inverse-video t))))
 '(fixed-pitch ((t (:family "Hack" :slant normal :weight normal :height 1.0 :width normal))))
 '(glyphless-char ((t (:height 0.7))))
 '(header-line ((t (:background "#303030" :foreground "#e7f6da" :height 1.2))))
 '(highlight-indentation-face ((t (:background "gray17"))))
 '(mode-line ((((class color) (min-colors 257)) (unspecified nil (:color foreground-color :style line) :inverse-video :foreground "#F8F8F0" :background "#49483E" :box (:line-width 1 :color "#64645E" :style unspecified))) (((class color) (min-colors 89)) (:inverse-video unspecified :underline t :foreground "#F5F5F5" :background "#1B1E1C" :box (:line-width 1 :color "#474747" :style unspecified)))))
 '(my-org-header ((t (:inherit default :family "EtBembo"))))
 '(org-block ((t (:inherit fixed-pitch))))
 '(org-checkbox ((t (:box nil))))
 '(org-code ((t (:inherit fixed-pitch :foreground "#75715E"))))
 '(org-default ((t (:inherit variable-pitch))))
 '(org-document-info ((t (:foreground "dark orange"))))
 '(org-document-info-keyword ((t (:inherit (shadow fixed-pitch)))))
 '(org-document-title ((t (\,@headline (\,@ variable-tuple) :height 2.0 :underline nil))))
 '(org-formula ((t (:inherit fixed-pitch :foreground "#E6DB74"))))
 '(org-headline-done ((t nil)))
 '(org-indent ((t (:inherit (org-hide fixed-pitch)))))
 '(org-link ((t (:foreground "deep sky blue" :underline t))))
 '(org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)))))
 '(org-property-value ((t (:inherit fixed-pitch))) t)
 '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
 '(org-table ((t (:inherit fixed-pitch :foreground "#A6E22E"))))
 '(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold :height 0.8))))
 '(org-variable-pitch-face ((t (:family "Hack"))) t)
 '(org-verbatim ((t (:inherit (shadow fixed-pitch)))))
 '(php-variable-sigil ((t nil)))
 '(variable-pitch ((t (:weight normal :height 140 :family "Source Sans Pro")))))

(put 'narrow-to-region 'disabled nil)
