#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from i3pystatus import Status

status = Status()

# Displays clock like this:
# Tue 30 Jul 11:59:46 PM KW31
#                          ^-- calendar week
status.register("clock",
                format="W%V, %a %-d %b %X",
                on_leftclick="gnome-calendar",)

# Shows the average load of the last minute and the last 5 minutes
# (the default value for format is used)
# status.register("load")

# Shows your CPU temperature, if you have a Intel CPU
status.register("temp",
                format="{temp:.0f}°C",
                hints = {'markup':'pango'},
)


status.register("cpu_usage",
                on_leftclick="gnome-terminal --title=htop -e 'htop'",
                hints = {'min_width': "  99%",},
                format="  {usage}%",)

status.register("mem",
    color="#999999",
    warn_color="#E5E500",
    alert_color="#FF1919",
                format=" {percent_used_mem}%",
                hints = {'markup':'pango'},
    divisor=1073741824,)

# The battery monitor has many formatting options, see README for details

# This would look like this, when discharging (or charging)
# ↓14.22W 56.15% [77.81%] 2h:41m
# And like this if full:
# =14.22W 100.0% [91.21%]
#
# This would also display a desktop notification (via D-Bus) if the percentage
# goes below 5 percent while discharging. The block will also color RED.
# If you don't have a desktop notification demon yet, take a look at dunst:
#   http://www.knopwob.org/dunst/
status.register("battery",
    format="{status} {percentage:.2f}%",
    alert=True,
    alert_percentage=10,
                hints = {'markup':'pango'},
    status={
        "DIS": "\uf242↓",
        "CHR": "\uf242↑",
        "FULL": "\uf240",
    },)


status.register("backlight",
    interval=5,
    format=" {percentage:.0f}%",
                hints = {'markup':'pango'},
    backlight="intel_backlight",)

# This would look like this:
# Discharging 6h:51m
# status.register("battery",
#     format="{status} {remaining:%E%hh:%Mm}",
#     alert=True,
#     alert_percentage=5,
#     status={
#         "DIS":  "Discharging",
#         "CHR":  "Charging",
#         "FULL": "Bat full",
#     },)

# Displays whether a DHCP client is running
# status.register("runwatch",
#     name="DHCP",
#     path="/var/run/dhclient*.pid",)

# Shows the address and up/down state of eth0. If it is up the address is shown in
# green (the default value of color_up) and the CIDR-address is shown
# (i.e. 10.10.10.42/24).
# If it's down just the interface name (eth0) will be displayed in red
# (defaults of format_down and color_down)
#
# Note: the network module requires PyPI package netifaces
# status.register("network",
#     interface="eth0",
#     format_up="{v4cidr}",)

# Note: requires both netifaces and basiciw (for essid and quality)
# status.register("network",
#     interface="wlp3s0",
#                 hints = {'markup':'pango'},
#     format_up="{essid}{quality:3.0f}%",)

# Shows disk usage of /
# Format:
# 42/128G [86G]
status.register("disk",
                path="/",
                hints = {'markup':'pango'},
                format="{avail}G",)

# Shows pulseaudio default sink volume
#
# Note: requires libpulseaudio from PyPI
status.register("pulseaudio",
    color_unmuted='#98C379',
    color_muted='#E06C75',
    format_muted=' [muted]',
                hints = {'markup':'pango'},
    format=" {volume}%")

# Shows mpd status
# Format:
# Cloud connected▶Reroute to Remain
# status.register("mpd",
#     format="{title}{status}{album}",
#     status={
#         "pause": "▷",
#         "play": "▶",
#         "stop": "◾",
#     },)


status.register("keyboard_locks",
    format='{caps} {num}',
    caps_on='Caps Lock',
    caps_off='',
    num_on='Num On',
    num_off='',
    color='#e60053',
                hints = {'markup':'pango',
                         'separator': False},
    )


status.register("window_title",
                format = "{title}",
                hints = {#'markup':'pango',
                         'align': 'left',
                         'min_width': 800,
                         'separator': False},
                empty_title = "~ no title ~")


status.run()
