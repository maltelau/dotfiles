#!/bin/bash

BRIGHT="/sys/class/backlight/intel_backlight/brightness"
MAX="/sys/class/backlight/intel_backlight/max_brightness"
AC="/sys/class/backlight/intel_backlight/actual_brightness"


case "$1" in
	"down") FACTOR=0.9;;
	"up")   FACTOR=1.1;;
	*)      exit 1;;
esac

curval=`cat $BRIGHT`
maxval=`cat $MAX`
newval=`echo "$curval * $FACTOR" | bc`

echo $curval
echo $newval

if [ $newval -gt $maxval ]; then
	newval=$maxval
elif [ $newval -lt 0 ]; then
	newval=0
fi

echo $newval

# cat $AC
# echo 500 | tee -a $BR
# cat $AC
