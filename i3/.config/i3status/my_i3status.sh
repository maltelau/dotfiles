
i3status | while :
do
    read line
    export GPUTEMP=`nvidia-smi -q -d TEMPERATURE | awk '/GPU Current Temp/{gsub(/[ \t]+/, " ", $0); print $5}'`
    export BGTITLE="`cat ~/.wallpaper/title.txt`"

    echo " $BGTITLE    GPU $GPUTEMP°    $line" || exit 1
done
