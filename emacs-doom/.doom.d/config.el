;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
;; (setq user-full-name "John Doe"
;;       user-mail-address "john@doe.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
;; (setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)
;; https://github.com/doomemacs/themes?tab=readme-ov-file
(setq doom-themes-treemacs-theme "doom-colors")
(doom-themes-treemacs-config)
(doom-themes-org-config)
(custom-set-faces
 `(font-lock-comment-face ((t (:foreground ,(doom-color 'base6))))))



;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")
;;(setq initial-buffer-choice (lambda () (interactive) (org-agenda nil "c")))
(setq initial-buffer-choice "~/org/TODO.org")
;; (add-hook 'focus-in-hook '(lambda () (org-agenda-list 1)))


;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; (use-package! stan-ts-mode)
;;
(defun my-poly-eval-skip (&optional vis)
  "custom version of `ess-eval-region-or-function-or-paragraph-and-step`"
  (interactive "P")
  ;; (ess-eval-region-or-function-or-paragraph vis)
  (ess-skip-thing (ess-eval-region-or-function-or-paragraph vis))
  ;; (polymode-next-chunk-same-type 1)
  (ess-next-code-line)
  )

(after! ess
  ;; (add-hook! 'prog-mode-hook #'rainbow-delimiters-mode)

  ;; If I use LSP it is better to let LSP handle lintr. See example in
  ;; https://github.com/hlissner/doom-emacs/issues/2606.
  (setq! ess-use-flymake nil)
  (setq! lsp-ui-doc-enable nil
         lsp-ui-doc-delay 1.5)

  ;; Code indentation copied from my old config.
  ;; Follow Hadley Wickham's R style guide
  (setq
   ess-style 'RStudio
   ess-offset-continued 4
   ess-expression-offset 0)

  ;; trying to show a minibuffer function definition is massively slow
  ;; https://emacs.stackexchange.com/questions/9621/how-to-find-what-causes-ess-to-run-very-slow
  ;; (setq ess-r--company-meta nil)

  ;; (setq comint-move-point-for-output t)
  (setq
   flycheck-lintr-linters
   "default_linters(closed_curly_linter = NULL, commas_linter = NULL, commented_code_linter = NULL, infix_spaces_linter = NULL, line_length_linter = NULL, object_length_linter = NULL, object_name_linter = NULL, object_usage_linter = NULL, open_curly_linter = NULL, pipe_continuation_linter = NULL, single_quotes_linter = NULL, spaces_inside_linter = NULL, spaces_left_parentheses_linter = NULL, trailing_blank_lines_linter = NULL, trailing_whitespace_linter = NULL, indentation_linter = NULL)")


  ;; From https://emacs.readthedocs.io/en/latest/ess__emacs_speaks_statistics.html
  ;; TODO: find out a way to make settings generic so that I can also set ess-inf-R-font-lock-keywords
  ;; (setq ess-R-font-lock-keywords
  ;;       '((ess-R-fl-keyword:modifiers  . t)
  ;;         (ess-R-fl-keyword:fun-defs   . t)
  ;;         (ess-R-fl-keyword:keywords   . t)
  ;;         (ess-R-fl-keyword:assign-ops . t)
  ;;         (ess-R-fl-keyword:constants  . t)
  ;;         (ess-fl-keyword:fun-calls    . t)
  ;;         (ess-fl-keyword:numbers      . t)
  ;;         (ess-fl-keyword:operators    . t)
  ;;         (ess-fl-keyword:delimiters) ; don't because of rainbow delimiters
  ;;         (ess-fl-keyword:=            . t)
  ;;         (ess-R-fl-keyword:F&T        . t)
  ;;         (ess-R-fl-keyword:%op%       . t)))

  ;; ESS buffers should not be cleaned up automatically
  (add-hook 'inferior-ess-mode-hook #'doom-mark-buffer-as-real-h)

  ;; Open ESS R window to the left iso bottom.
  (set-popup-rule! "^\\*R.*\\*$" :side 'right :size 0.38 :select nil :ttl nil :quit nil :modeline t))

(use-package! polymode
  :commands (R)
  :bind (:map polymode-minor-mode-map
              ("C-c C-c" . my-poly-eval-skip)))

(setq-hook! ess-r-mode polymode-lsp-integration nil)
(setq-hook! markdown-mode polymode-lsp-integration nil)

;; (use-package! lsp-mode)

;; (use-package! org-variable-pitch
;;   :hook (org-mode . org-variable-pitch-minor-mode)
;;   :config
;;   ;; add -> to variable pitch mode
;;   (setq org-variable-pitch-font-lock-keywords
;; 	;; taken from org-variable pitch
;; 	(let ((code '(0 (put-text-property
;; 			 (match-beginning 0)
;; 			 (match-end 0)
;; 			 'face 'org-variable-pitch-face))))
;; 	  `((,(rx bol (1+ blank))
;; 	     ,code)
;; 	    (,(rx bol (0+ blank)
;; 		  (or (: (or (+ digit) letter) (in ".)"))
;; 		      (: (in "-+")
;; 			 (opt ">") ;; modified: -> rightarrow
;; 			 (opt blank "[" (in "-X ") "]"))
;; 		      (: (1+ blank) "\*"))
;; 		  blank)
;; 	     ,code)))))

;; (after! markdown-mode
;;   ;; Disable trailing whitespace stripping for Markdown mode
;;   (add-hook 'markdown-mode-hook #'doom-disable-delete-trailing-whitespace-h)
;;   ;; Doom adds extra line spacing in markdown documents
;;   (add-hook! 'markdown-mode-hook :append (setq line-spacing nil)))

;; ;; From Tecosaur's configuration
;; (add-hook! (gfm-mode markdown-mode) #'mixed-pitch-mode)

;; (cfw:open-ical-calendar "https://mitstudie.au.dk/calendar?icalkey=1466a7d4c5e01bea9eb117f03d015971&lang=da&getLmsEvents=false")
;; (setq-hook! cfw:display-calendar-holidays nil)
;;
(defun my-open-calendar ()
  (interactive)
  (cfw:open-calendar-buffer
   :contents-sources
   (list
    ;; (cfw:org-create-source "Green")  ; org-agenda source
    ;; (cfw:org-create-source "activities" "~/org/activities.org" "Grey")  ; org-agenda source
    (cfw:org-create-file-source "activities" "~/org/activities.org" "Green")  ; other org source
    (cfw:org-create-file-source "au" "~/org/au.org" "red")  ; other org source
    ;; (make-cfw:source :name "Org: au" :data (lambda (begin end) (cfw:org-to-calendar "~/org/au.org" begin end)) :period-fgcolor "red")
    (cfw:org-create-file-source "todo" "~/org/TODO.org" "blue")  ; other org source
    (cfw:org-create-file-source "reading" "~/org/reading.org" "goldenrod")  ; other org source
    ;; (cfw:org-create-file-source "decmak" "~/org/read-decis.org" "goldenrod")  ; other org source
    ;; (cfw:org-create-file-source "nlp" "~/org/read-nlp.org" "goldenrod")  ; other org source
    (cfw:org-create-file-source "imc" "~/org/imc.org" "light blue")
    ;; (cfw:org-create-file-source "cal" "~/org/activities.org" "Cyan")  ; other org source
    ;; (cfw:howm-create-source "Blue")  ; howm source
    ;; (cfw:cal-create-source "Orange") ; diary source
    ;; (cfw:ical-create-source "Moon" "~/moon.ics" "Gray")  ; ICS source1
    ;; (cfw:ical-create-source "AU timetable" "https://timetable.au.dk/ical?64f39c6f&group=false&eu=NTQwMDQx&h=ytHEfEQte0TJ71Bu6TMfK8pNMRgW9toFrAsk0Of3O_8=" "Green")
    )
   :view 'transpose-two-weeks))


(use-package! calfw
  :bind ("C-c o c" . my-open-calendar)
  :config
  (setq cfw:display-calendar-holidays nil)
  (setq calendar-week-start-day 1))

(use-package! calfw-blocks
  :after calfw)

(after! org (setq org-cite-global-bibliography '("~/Dokumenter/zotero.bib")))

(use-package! org-super-agenda
  :after org-agenda
  :init
  (setq org-agenda-skip-scheduled-if-done t
        org-agenda-skip-deadline-if-done t
        org-agenda-include-deadlines t
        org-agenda-block-separator nil
        org-agenda-compact-blocks t
        org-agenda-start-day nil ;; i.e. today
        org-agenda-span 'week
        org-agenda-start-on-weekday nil
        org-todo-keywords '((list "TODO" "DONE")
                            (list "MEET" "PROJ" "LOOP" "STRT" "WAIT" "HOLD" "IDEA" "KILL")
                            (list "[ ]" "[-]" "[?]" "[X]")
                            (list "OKAY" "YES" "NO")))
  (setq org-agenda-custom-commands
        '(("c" "Super view"
           ((agenda "" ((org-agenda-overriding-header "")
                        (org-agenda-span 2)
                        (org-agenda-start-day nil)
                        (org-super-agenda-groups
                         '(
                           (:name "Overdue"
                            :and (:not (:todo "DONE")
                                  :deadline past
                                  ;; :not (:priority "A")
                                  )
                            :order 0)
                           (:name "Due today"
                            :and (:deadline today
                                  :not (:todo "DONE"))
                            :order 1)
                           (:name "Schedule"
                            :time-grid t
                            :and (:date today
                                  :deadline nil)
                            :order 3)
                           ;; (:log t)
                           ;; (:name "To refile"
                           ;;  :file-path "refile\\.org")
                           ;; (:name "Next to do"
                           ;;  :todo "NEXT"
                           ;;  :order 1)
                           ;; ;; (:name "Important"
                           ;; ;;  :priority "A"
                           ;; ;;  :order 1)
                           ;; (:name "Today's tasks"
                           ;;  :file-path "journal/")
                           (:name #("-> Due this week\n" 0 1
                                    (rear-nonsticky t display (raise -0.24) font-lock-face (:family "Material Icons" :height 1.2)
                                                    face (:family "Material Icons" :height 1.2)))
                            :and (:deadline future
                                  :not (:todo "DONE"))
                            :order 2)
                           (:discard (:anything t))
                           ;; (:discard (:scheduled past :todo "MEET"))
                           ;; (:discard (:anything t))
                           ;; (:discard (:not (:todo "TODO")))
                           ;; (:discard (:priority "A"))
                           ))))
            ;; (search "+reading+DEADLINE=\"<+2w>\"" ((org-agenda-overriding-header "")
            ;;                              (org-agenda-span 14)
            ;;                              (org-super-agenda-groups
            ;;                               '(
            ;;                                 (:name #("Reading due soon\n" 0 1
            ;;                                          (rear-nonsticky t display (raise -0.24) font-lock-face (:family "Material Icons" :height 1.2)
            ;;                                                          face (:family "Material Icons" :height 1.2)))
            ;;                                  :and (:deadline future
            ;;                                        :not (:todo "DONE"))
            ;;                                  :order 2)
            ;;                                 ;; (:discard (:anything t))
            ;;                                 ))))
            (alltodo "" ((org-agenda-overriding-header "")
                         (org-agenda-span 'week)
                         (org-super-agenda-groups
                          '(;; (:name "Upcoming"
                            ;;  ;; :time-grid t
                            ;;  :scheduled future)
                            ;; (:name "Important"
                            ;;  :and (:priority "A"
                            ;;        :not (:todo "DONE"))
                            ;;  :order 0)

                            (:name "Todo-list"
                             :priority<= "A"
                             :todo "TODO"
                             :order 5)
                            ;; (:discard (:anything t))
                            ;;  :order 4)
                            ))))))))
  :config
  (org-super-agenda-mode))
(use-package! notmuch
  :config
  (setq +notmuch-home-function (lambda () (notmuch-search "tag:inbox")))
  (setq org-mime-export-options '(:with-latex dvipng
                                  :section-numbers nil
                                  :with-author nil
                                  :with-toc nil))
  (add-hook 'message-send-hook 'org-mime-confirm-when-no-multipart))

(use-package! message
  :config
  (setq message-cite-style message-cite-style-outlook))


(use-package! gnus-alias
  :ensure t
  :defer t
  :hook (message-setup . gnus-alias-determine-identity)
  :init
  (setq gnus-alias-identity-alist
	'(("pm" nil "Malte Lau Petersen <maltelau@protonmail.com>")
	  ("dk" nil "Malte Lau Petersen <maltelau@mlpetersen.dk>")
	  ("au" nil "Malte Lau Petersen <201505524@post.au.dk>")))
  (setq gnus-alias-default-identity "dk")
  (setq gnus-alias-identity-rules
	'(("au" ("any" "201505524@post\\.au\\.dk" both) "au")
	  ("pm" ("any" "maltelau@protonmail\\.com" both) "dk")
	  ("dk" ("any" "maltelau@mlpetersen\\.dk" both) "dk"))))

(setq send-mail-function 'sendmail-send-it
      sendmail-program "/usr/bin/msmtp"
      mail-specify-envelope-from t
      message-sendmail-envelope-from 'header
      mail-envelope-from 'header)

(use-package! tramp
  :config
  (setq tramp-ssh-controlmaster-options nil)
  ;; (concat
  ;;  "-o ControlPath=/tmp/ssh-ControlPath-%%r@%%h:%%p "
  ;;  "-o ControlMaster=auto -o ControlPersist=yes"))
  )


(setq read-process-output-max (* 1024 1024))
(setq lsp-use-plists "true")

(use-package! combobulate
  :preface
  (setq combobulate-key-prefix "C-c o")
  :hook ((python-ts-mode . combobulate-mode)
         (js-ts-mode . combobulate-mode)
         (css-ts-mode . combobulate-mode)
         (yaml-ts-mode . combobulate-mode)
         (json-ts-mode . combobulate-mode)
         (typescript-ts-mode . combobulate-mode)
         (tsx-ts-mode . combobulate-mode)))


(use-package! pandoc-mode
  :config
  (setq org-latex-pdf-process
	'("latexmk -pdflatex='pdflatex -shell-escape -interaction=nonstopmode' -pdf -bibtex %b -f %f"))
  (setq reftex-default-bibliography  '("/home/maltelau/Dokumenter/zotero.bib"))
  (setq org-ref-default-bibliography '("/home/maltelau/Dokumenter/zotero.bib"))
  (setq org-ref-bibtex-files         '("/home/maltelau/Dokumenter/zotero.bib"))
  (setq org-latex-src-block-backend 'minted))
;; C-c C-l python-shell-send-buffer
;; C-c C-c python-shell-send-chunk ;; DOESNT EXIST??

(after! ox-latex
  (add-to-list 'org-latex-packages-alist '("" "minted"))
  )

(add-to-list 'revert-without-query '".pdf")

(after! org
  (require 'ox-latex)
  (setq org-export-in-background t)
  (add-to-list 'org-latex-classes
	       '("apa7" "
\\documentclass[hidelinks,a4paper,jou,biblatex,12pt,longtable]{apa7}
\\renewcommand{\\baselinestretch}{1.4}"
		 ("\\section{%s}" . "\\section*{%s}")
		 ("\\subsection{%s}" . "\\subsection*{%s}")
		 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
		 ("\\paragraph{%s}" . "\\paragraph*{%s}")
		 ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
  (add-to-list 'org-latex-classes
	       '("apa7-phdapp" "
\\documentclass[hidelinks,a4paper,doc,biblatex,12pt,longtable]{apa7}
\\renewcommand*{\\mkbibnamegiven}[1]{%
\\ifitemannotation{highlight}
{\\textbf{#1}}
{#1}}
\\renewcommand*{\\mkbibnamefamily}[1]{%
\\ifitemannotation{highlight}
{\\textbf{#1}}
{#1}}"
		 ("\\section{%s}" . "\\section*{%s}")
		 ("\\subsection{%s}" . "\\subsection*{%s}")
		 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
		 ("\\paragraph{%s}" . "\\paragraph*{%s}")
		 ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))


(defun my-org-export-pdf-async ()
  (org-latex-export-to-pdf t))


(defun toggle-org-pdf-export-on-save ()
  (interactive)
  (if (memq 'org-latex-export-to-pdf after-save-hook)
      (progn
        (remove-hook 'after-save-hook 'my-org-export-pdf-async t)
        (message "Disabled org export on save for current buffer..."))
    (add-hook 'after-save-hook 'my-org-export-pdf-async nil t)
    (message "Enabled org export on save for current buffer...")))

(use-package! citar
  :custom
  (citar-bibliography org-cite-global-bibliography)
  :hook
  (LaTeX-mode . citar-capf-setup)
  (org-mode . citar-capf-setup)
  :bind
  (:map org-mode-map :package org ("C-c b" . #'org-cite-insert)))


(use-package! org-notmuch
  :bind (("C-x m" . notmuch)
	 :map notmuch-search-mode-map
	 ("S" . (lambda () (interactive) (notmuch-search-tag (list "+spam" "-inbox" "-unread"))))
	 ("D" . (lambda () (interactive) (notmuch-search-tag (list "+deleted" "-inbox" "-unread"))))
	 ("R" . (lambda () (interactive) (notmuch-search-tag (list "-unread" "-todo"))))
	 ("U" . (lambda () (interactive) (notmuch-search-tag (list "+unread"))))
	 ("T" . (lambda () (interactive) (notmuch-search-tag (list "+todo"))))
	 :map notmuch-show-mode-map
	 ("S" . (lambda () (interactive) (notmuch-show-tag (list "+spam" "-inbox" "-unread"))))
	 ("D" . (lambda () (interactive) (notmuch-show-tag (list "+deleted" "-inbox" "-unread"))))
	 ("R" . (lambda () (interactive) (notmuch-show-tag (list "-unread" "-todo"))))
	 ("U" . (lambda () (interactive) (notmuch-show-tag (list "+unread"))))
	 ("T" . (lambda () (interactive) (notmuch-show-tag (list "+todo"))))
	 ("C-<return>" . (lambda () (interactive) (let ((current-prefix-arg 4))
	    	                                    (call-interactively 'notmuch-show-open-or-close-all 'nil)))))
  )
;; :after (org notmuch))

;; (use-package! flyspell
;;   :config
;;   (ispell-change-dictionary "english")
;;   (setq flycheck-global-modes  '(org-mode)))

;; (use-package! org-wc
;;   :after org)


(use-package! spell-fu
  :after ispell
  :config
  (add-to-list 'ispell-skip-region-alist '("^```" . "```$")))
(add-hook 'spell-fu-mode-hook
          (lambda ()
            (spell-fu-dictionary-add (spell-fu-get-ispell-dictionary "en"))
            (spell-fu-dictionary-add (spell-fu-get-ispell-dictionary "da"))
            (spell-fu-dictionary-add
             (spell-fu-get-personal-dictionary "en-personal" "/home/maltelau/.local/share/spelling/aspell.en.pws"))
            (spell-fu-dictionary-add
             (spell-fu-get-personal-dictionary "da-personal" "/home/maltelau/.local/share/spelling/aspell.da.pws"))))


;; (use-package copilot
;;   :init
;;   ;; (setq! copilot-bin "/home/maltelau/.local/lib/llama.cpp/models/codellama-7b-instruct.Q4_K_M.gguf")
;;   (setq! copilot-bin "/home/maltelau/.local/lib/llama.cpp/models/phi-2.Q5_K_M.llamafile")
;;   )

(use-package! nose
  :init
  (setq! nose-global-name "nosetests3"))

(use-package! py-isort
  :hook (before-save . py-isort-before-save))

(use-package! pyimport
  :init
  (setq! pyimport-pyflakes-path
         (executable-find "pyflakes3")))

;; (use-package! lsp-mode)
(use-package! pdx
  ;; :after lsp-mode
  :hook (pdx-mode . lsp)
  :mode ("/Europa Universalis IV/.*\\.txt\\'" . pdx-mode)
  )

(use-package! pdf-tools
  :magic ("%PDF" . pdf-view-mode)
  :config
  (pdf-tools-install :no-query))

(use-package! magit
  :config
  (setq! magit-diff-refine-hunk (quote all)))

(use-package! lsp
  :config
  (setq lsp-ui-doc-show-with-cursor t)
  (setq lsp-headerline-breadcrumb-enable t)
  (setq lsp-ui-sideline-enable t)
  (setq lsp-ui-sideline-show-hover t))

(use-package! apheleia
  :config
  (push '(styler . ("Rscript"
                    "/home/maltelau/.local/bin/styler.R"
                    filepath))
        apheleia-formatters)
  (setf (alist-get 'ess-r-mode apheleia-mode-alist)
        '(styler)))

(use-package! pyenv-mode
  :init
  (let ((pyenv-path (expand-file-name "~/.pyenv/bin")))
    (setenv "PATH" (concat pyenv-path ":" (getenv "PATH")))
    (setenv "WORKON_HOME" (expand-file-name "~/Envs"))
    (add-to-list 'exec-path pyenv-path)))

(use-package! python
  :init
  (defun run-python-once (&rest r)
    "Call run-python if the *Python* buffer doesn't already exist"
    (interactive)
    (let (b (current-buffer))
      (unless (python-shell-get-process)
        (call-interactively 'run-python))
      ;;(switch-to-buffer b)
      )) ;;(python-shell-calculate-command))))
  (advice-add 'python-shell-send-buffer :before 'run-python-once)
  ;; (advice-add 'python-shell-send-file :before 'run-python-once)
  ;; (advice-add 'python-shell-send-region :before 'run-python-once)
  ;; (advice-remove 'python-shell-send-region 'run-python-once))
  )
