# to be executed on the device itself
# quickNotesLayersToWiki(){ cd xochitl-data; curl -d "?n=PIMVRdata.ReMarkableQuicknotes&text=$(grep name UUID/*json | sed 's/.*: "\(.*\)"/%0a* \1/' | tr '\n' ' ')&action=edit&post=1&author=reMarkable2Ttest&authpw=edit_pw_to_change" https://fabien.benetou.fr/PIMVRdata/ReMarkableQuicknotes; cd -; }
# # note that you must replace UUID and edit_pw by the UUID of your quicknote document (you can use hashFromTitle() for that) and your PmWiki password respectively.
# qrCodeLink () { qrencode http://`ip a | grep wlan0$ | sed "s/.* \(.*\)\/.*/\1/"`:3000/data/$1.pdf -o qr_ip.xpm -t XPM && convert qr_ip.xpm qr_ip.pdf && addPdfWithMetadata qr_ip.pdf; }
# hashFromTitle () { cd ~/.local/share/remarkable/xochitl/ && grep -l -i $1 *metadata | sed 's/.metadata//'; }
# addPdfWithMetadata(){ cp $1 ~/.local/share/remarkable/xochitl/ && echo "{'parent':'','type':'DocumentType','visibleName':'$1'}" | sed s/\'/\"/g > ~/.local/share/remarkable/xochitl/`echo $1 | sed "s/.pdf//"`.metadata; }
# findBookmarkedPages() {
#     hash=$(hashFromTitle $1)
#     cd xochitl-data;
#     for b in $(grep -l -o 'bkm\|bookmark' $hash/*.json | sed 's/.*\/\(.*\)-metadata.*/\1/'); do
#         jq ".pages[] | test(\"$b\")" $hash.content | grep -n true | sed s/:.*//;
#     done;
# }
# findBookmarks() {
#     rm bkm_$1.* xochitl-data/bkm_$1* -rf
#     findBookmarkedPages $1 > bkm_$1.txt
#     cd
#     convert -size 1404x1872 -gravity east -fill black caption:@bkm_$1.txt bkm_$1.png
#     convert bkm_$1.png bkm_$1.pdf
#     addPdfWithMetadata bkm_$1.pdf
#     echo `cat bkm_$1.txt | wc -l` bookmarks found, open bkm_$1.pdf on the device to see page numbers
# }

# to be executed remotely
lastReadDocument () { ssh $1 'cd ~/.local/share/remarkable/xochitl/; LASTBOOK=`ls -rth *metadata | tail -1 | sed 's/.metadata//'`; grep visibleName $LASTBOOK.metadata; grep lastOpenedPage $LASTBOOK.content'; }
hashFromTitle () { ssh $1 "cd ~/.local/share/remarkable/xochitl/; grep -l -i \"$2\" *metadata | sed 's/.metadata//'"; }
lastPageFromTitle () { ssh $1 "cd ~/.local/share/remarkable/xochitl/; grep lastOpenedPage `hashFromTitle $1 $2`.content | sed 's/.* \([0-9]\+\),.*/\1/';"; }
syncGreatest () {
        RM1=remarkable
        RM2=remarkable2
        RM1p=`lastPageFromTitle $RM1 $1`
        RM2p=`lastPageFromTitle $RM2 $1`
        HASH=`hashFromTitle $RM1 $1` # assumes the same hash! Should at least check
        if [ $RM1p -gt $RM2p ];
                then
                        echo rm1 greater;
                        ssh $RM2 "cd ~/.local/share/remarkable/xochitl/; cat $HASH.content | sed 's/$RM2p/$RM1p/' > new; mv new $HASH.content; systemctl restart xochitl;";
                else
                        echo rm2 greater;
                        ssh $RM1 "cd ~/.local/share/remarkable/xochitl/; cat $HASH.content | sed 's/$RM1p/$RM2p/' > new; mv new $HASH.content; systemctl restart xochitl;";
        fi
}
copyAccrossRms () { scp -3 -r $1:~/xochitl-data/`hashFromTitle $1 $3`* $2:~/xochitl-data/ && ssh $2 systemctl restart xochitl; }
