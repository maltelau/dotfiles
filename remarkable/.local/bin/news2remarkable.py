#!/bin/env python3
import feedparser
import os
from bs4 import BeautifulSoup
import requests
import tempfile
import time
import subprocess
import string

feed = "https://www.dr.dk/nyheder/service/feeds/senestenyt"
downloadfolder = os.path.join(os.path.expanduser("~/"),"mydownload")


def remove_tag(soup, cls, tag="div"):
    for t in soup.find_all(tag, {'class': cls}):
        t.decompose()

def pretty_name(name):
    valid_chars = "-_.() {0}{1}".format(string.ascii_letters, string.digits)
    valid_filename = "".join(ch for ch in name if ch in valid_chars)
    return valid_filename

def hash_from_title(title):
    return subprocess.check_output(f'ssh remarkable "cd /home/root/.local/share/remarkable/xochitl/; grep -l -i \'{title}\' *metadata | sed \'s/.metadata//\'"', shell=True)[:-1].decode("utf-8")

def fix_remarkable_filename(name, folder_hash = ""):
    h = hash_from_title(name)
    os.system(f'ssh remarkable "cd /home/root/.local/share/remarkable/xochitl/; sed -ri \'s/({name})\\.epub/\\1/g\' {h}.metadata"')

    cmd = f"""ssh remarkable "sed -i \'s/\\"parent\\": \\"\\"/\\"parent\\": \\"{folder_hash}\\"/g\' \\"/home/root/.local/share/remarkable/xochitl/{h}.metadata\\"" """
    print(cmd)
    os.system(cmd)

feed = feedparser.parse(feed)

with tempfile.TemporaryDirectory() as tmp:
    os.chdir(tmp)
    epubs = []
    
    for e in feed['entries']:
        title = pretty_name(e['title'])
        if hash_from_title(title):
            print(f"Article already on remarkable: {title}")
            continue
        else:
            print(f"Downloading article: {title}")
        article = e['link']

        soup = BeautifulSoup(requests.get(article).content, "html.parser")
        remove_tag(soup, 'dre-top-navigation', tag="nav")
        remove_tag(soup, "dre-theme-header-band")
        remove_tag(soup, "dre-article-title__section-label")
        remove_tag(soup, "dre-article-byline__tools")
        remove_tag(soup, "dre-article-body-quote__icon")
        remove_tag(soup, "dre-text-selection-toolbar", tag = "span")
        remove_tag(soup, "dre-share-band")
        remove_tag(soup, "dre-global-footer", tag = "footer")
        
        epubname = tmp + "/" + title + ".epub"
        htmlname = tmp + "/" + title + ".html"
        with open(htmlname, "w") as f:
            f.write(str(soup))
            os.system(f'ebook-convert "{htmlname}" "{epubname}" --no-default-epub-cover --output-profile=generic_eink_large --page-breaks-before="//*[name()=\'h1\' and @class = \'dre-article-title__heading\']"')


        os.system(f'curl --form file=@"{epubname}" http://10.11.99.1:80/upload')


    time.sleep(10)

    newshash = hash_from_title("Nyheder")
    if newshash:
        for e in feed['entries']:
            title = pretty_name(e['title'])
            article = e['link']
            fix_remarkable_filename(title, folder_hash = newshash)
    else:
        print("'Nyheder' folder not found")



# sed -i 's/"parent": ""/"parent": "LOL"/g' "~/.local/share/remarkable/xochitl/5930c9b6-67ab-4753-bcb9-310052e5ce89.metadata"
