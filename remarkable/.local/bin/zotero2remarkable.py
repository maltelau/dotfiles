#!/bin/env python3
import os

zotero_file = "/home/maltelau/.local/share/Remarkable.bib"

with open(zotero_file) as f:
    bibt = f.readlines()

# print(bibt)
files = [['"'+f2+'"' for f2 in f.split("{")[1].split("}")[0].split(";") if "pdf" in f2][0]  for f in bibt if "file =" in f]
command = f"pdf2remarkable.sh -r {' '.join(files)}"

# print(command)
os.system(command)
